/*
	Author Peyman Barjoueian
	Email: promise7091@Gmail.com
	Web site: http://wwww.Barjoueian.com
*/

import java.util.concurrent.Semaphore;
import java.io.*;

class DiningPhilosophers{

	static Semaphore mutex = new Semaphore(1);
	static Semaphore next = new Semaphore(1);
	static int next_count = 0;
	static DP dp = null;
	static PrintWriter out = null;
	static boolean showOutput = false;
	static boolean showThinking = false;
	
	static void printUsage(){
		System.out.print(
			"\nUsage: java DiningPhilosophers <numP>" +
			" <num eats> <filename> [p] [t]\n\n" +
			"\tnumP\t\tnumber of philosophers that will eat\n\n" +
			"\tnum eats\tnumber of times each philosopher will eat\n\n" +
			"\tfilename\t(optional) the file that the results will be\n" +
			"\t\toutput to, if unspecified outputs to DPoutput.txt\n\n" +
			"\t[p]\t\t(optional) typing p will cause\n" +
			"\t\toutput to go to the screen as well as the file\n\n" +
			"\t[t]\t\t(optional) typing t will enable the output\n" +
			"\t\tthat shows when the philosophers are thinking\n\n" );
			
			System.exit(0);
	}
	
	public static void main(String... args) throws Exception{
	
		/* the next semaphore is supposed to be initialized
			to 0 according to the book, which basically means
			that the first thread that calls a wait on it
			will actually have to wait.  If it were initialized
			to 1 then the first thread that called it would not have
			to wait.  Here I am doing the same thing as initializing
			the semaphore next to 0 */
		try{
			next.acquire();
		}
		catch(Exception e){}
	
		// begin processing command line arguments 
		
		// number of dining philosophers, init to 0
		int numPHD = 0;
		
		// number of times they should each eat, init to 0
		int timesToEat = 0;
		
		if(args.length==2){
			try{
				numPHD = Integer.parseInt(args[0]);
				timesToEat = Integer.parseInt(args[1]);
			}	
			catch(Exception e){ printUsage(); }
			
			out = new PrintWriter(new FileWriter("DPoutput.txt"));
		}
		else if(args.length==3){
			try{
				numPHD = Integer.parseInt(args[0]);
				timesToEat = Integer.parseInt(args[1]);
			}	
			catch(Exception e){ printUsage(); }
		
			if(args[2].equals("p")){
				showOutput = true;
				out = new PrintWriter(new FileWriter("DPoutput.txt"));
			}
			if(args[2].equals("t")){
				showThinking = true;
				out = new PrintWriter(new FileWriter("DPoutput.txt"));
			}
			else out = new PrintWriter(new FileWriter(args[2]));
		}	
		else if(args.length==4){
			try{
				numPHD = Integer.parseInt(args[0]);
				timesToEat = Integer.parseInt(args[1]);
			}	
			catch(Exception e){ printUsage(); }
		
			if(args[2].equals("p") && args[3].equals("t")){
				showThinking = true;
				showOutput = true;
				out = new PrintWriter(new FileWriter("DPoutput.txt"));
			}
			else if(!args[2].equals("p") && args[3].equals("t")){
				out = new PrintWriter(new FileWriter(args[2]));
				showThinking = true;
			}
			else if(!args[2].equals("p") && args[3].equals("p")){
				out = new PrintWriter(new FileWriter(args[2]));
				showOutput = true;
			}
			else printUsage();
		}
		else if(args.length==5){
			try{
				numPHD = Integer.parseInt(args[0]);
				timesToEat = Integer.parseInt(args[1]);
			}	
			catch(Exception e){ printUsage(); }
		
			out = new PrintWriter(new FileWriter(args[2]));
				
			if(args[3].equals("p") && args[4].equals("t")){
				showOutput = true;
				showThinking = true;
			}
			else printUsage();
		}
		else printUsage();
		// end processing command line arguments
		
		//make one monitor
		dp = new DP(numPHD);
		
		// array of dining philosophers
		Philosopher[] p = new Philosopher[numPHD];
		
		// initialize each philosopher, telling him how many
		// times to eat and what his index id (i) is
		for(int i=0;i<numPHD;i++){
			p[i] = new Philosopher(i, timesToEat);
		
			if(showThinking){
				out.println("thread " + (i+1) + " is thinking");
				out.flush();
				
				if(showOutput)
					System.out.println("thread " + (i+1) + " is thinking");
			}
		
			p[i].start();	
		}
	}
	
	static class Philosopher extends Thread{
	
		Philosopher(int i, int timesToEat){
			this.i = i;
			this.timesToEat = timesToEat;
		}
		
		public void run(){
		
			for(int a=0;a<timesToEat;a++){
				
				//would rather say thread 1 than thread 0
				out.println("thread " + (i+1) + " hungry");
				out.flush();
				
				if(showOutput)
					System.out.println("thread " + (i+1) + " hungry");
				
				// attempt to enter the monitor, will block on
				// mutex.acquire() if another thread is in monitor
				try{
					mutex.acquire();
				}
				catch(Exception e){}
				
				//now inside the monitor
				dp.pickup(i);
				
				out.println("thread " + (i+1) + " is eating");
				out.flush();
				
				if(showOutput)
					System.out.println("thread " + (i+1) + " is eating");
				
				//leave the monitor
				if(next_count>0)
					next.release();
				else
					mutex.release();
									
				//eat for random time between 0 and EAT_TIME_MS seconds
				try{
					this.sleep((int)(EAT_TIME_MS*Math.random()));
				}
				catch(Exception e){}
				
				//attempt to enter the monitor, wait if it is occupied
				try{
					mutex.acquire();
				}
				catch(Exception e){};
			
				//now inside the monitor
				if(showThinking){
					out.println("thread " + (i+1) + " is thinking");
					out.flush();
					
					if(showOutput)
						System.out.println("thread " + (i+1) + " is thinking");
				}
				
				dp.putdown(i);
				
				//leave the monitor
				if(next_count>0)
					next.release();
				else
					mutex.release();
				
				//think for a while
				try{
					this.sleep((int)(EAT_TIME_MS*Math.random()));
				}
				catch(Exception e){}
			}
		}
		
		private int i;
		private int timesToEat;
		private final int EAT_TIME_MS = 100;
	}
	
	static enum State {eating, thinking, hungry};
	
	//there is only one instance of this class for the program.
	//it represents the monitor in that only one thread can be
	//using the methods of the only instance of DP at a time
	static class DP{
	
		DP(int numPHD){
		
			this.numPHD = numPHD;
			self = new Condition[numPHD];
			state = new State[numPHD];
			
			for(int i=0;i<numPHD;i++){
				state[i] = State.thinking;
				self[i] = new Condition();
			}	
		}
	
		void pickup(int i){
			state[i] = State.hungry;
			test(i);
			if(state[i] != State.eating)
				self[i].myWait();
		}
		
		void putdown(int i){
			state[i] = State.thinking;
			test((i + numPHD - 1)%numPHD);
			test((i + 1)%numPHD);
		}
		
		void test(int i){
			if((state[(i+numPHD-1)%numPHD]!=State.eating) &&
				(state[i] == State.hungry) &&
				(state[(i+1)%numPHD]!=State.eating)){
			
				state[i] = State.eating;
				self[i].signal();	
			}
		}
		
		private Condition[] self;
		private State[] state;
		private int numPHD;
	}
	
	//refer to text
	static class Condition{
	
		Condition(){
			x_sem = new Semaphore(1);
			
			//same as initializing x_sem to zero
			try{
				x_sem.acquire();
			}
			catch(Exception e){}
			
			x_count = 0;
		}
	
		//would have called it wait but in java not allowed
		//to override Object.wait()
		void myWait(){
			x_count++;
			if(next_count>0)
				next.release();
			else
				mutex.release();
			try{
			x_sem.acquire();
			}
			catch(Exception e){}
			x_count--;
		}
		
		void signal(){
			if(x_count>0){
				next_count++;
				x_sem.release();
				try{
					next.acquire();
				}
				catch(Exception e){}
				next_count--;
			}
		}
	
		private int x_count;
		private Semaphore x_sem;
	}
}